# SK_PSC
Geo souřadnice většiny slovenských PSČ

V CSV souboru najdete geo souřadnice (zeměpisnou délku a šířku) k jednotlivým PSČ Slovenské republiky. Soubor má kódování znaků win-1250.

Jako zdroj dat byl využit seznam obcí na SK wikipedii (https://sk.wikipedia.org/wiki/Zoznam_slovensk%C3%BDch_obc%C3%AD_a_vojensk%C3%BDch_obvodov)

Pokud na jedno PSČ připadlo více obcí, tak byla zeměpisná délka a šířka spočítána jako střed mezi obcemi s největší a nejmenší délkou, šířkou.

Seznam PSČ není kompletní - oproti seznamu na stránkách Slovenské pošty jich cca 100 chybí.

Další soubory viz: http://honya.cz/doku.php?id=geo_lokace_slovenskych_psc
